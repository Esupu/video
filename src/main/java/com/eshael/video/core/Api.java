package com.eshael.video.core;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eshael.video.persistance.Persistance;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Servlet implementation class Api
 */
public class Api extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Persistance persistance = new Persistance();
	private ObjectMapper objectMapper  = new ObjectMapper();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Api() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		String topic = request.getParameter("topic");

		switch (topic) 
		{
			case "categories":
				objectMapper.writeValue(out, persistance.listCategories());
				out.flush();
			break;
			case "videos":
				objectMapper.writeValue(out, persistance.listvideos());
				out.flush();
			break;
			default:
			break;
		}
	}
	

}
