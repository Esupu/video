package com.eshael.video.core;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.eshael.video.model.Video;
import com.eshael.video.persistance.Persistance;
import com.google.gson.Gson;

/**
 * Servlet implementation class Upload
 */

public class Upload extends HttpServlet {
	private static final long serialVersionUID = 1L;
//	private final String UPLOAD_DIRECTORY = "C:\\xampp\\tomcat\\webapps\\uploads\\";
	private final String UPLOAD_DIRECTORY=System.getenv().get("OPENSHIFT_DATA_DIR")+"/uploads/";
	
	String name;
	private Persistance persistance = new Persistance();
	private boolean isMultipart;
	private String filePath;
	private int maxFileSize = 1000 * 1024;
	private int maxMemSize = 1 * 1024;
	private File file ;   
	private Gson  gson  = new Gson();
	private int BUFFER_LENGTH = 4096;
	
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Upload() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init( ){
        // Get the file location where it would be stored.
        filePath = UPLOAD_DIRECTORY;//getServletContext().getInitParameter("file-upload"); 
     }
    

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("rawtypes")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		isMultipart = ServletFileUpload.isMultipartContent(request);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        Video video = new Video();
        video.setTitle(request.getParameter("title"));
        video.setDuration(request.getParameter("duration"));
        video.setDescription(request.getParameter("description"));
        video.setCategory(request.getParameter("category"));
         
        if( !isMultipart ){
           out.println("<html>");
           out.println("<head>");
           out.println("<title>Servlet upload</title>");  
           out.println("</head>");
           out.println("<body>");
           out.println("<p>No file uploaded</p>"); 
           out.println("</body>");
           out.println("</html>");
           return;
        }
        
        DiskFileItemFactory factory = new DiskFileItemFactory();
        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);
        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File(UPLOAD_DIRECTORY));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);
        // maximum file size to be uploaded.
//        upload.setSizeMax( maxFileSize );

        try
        { 	
        // Parse the request to get file items.
        List fileItems = upload.parseRequest(request);
  	
        // Process the uploaded file items
        Iterator i = fileItems.iterator();

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet upload</title>");  
        out.println("</head>");
        out.println("<body>");
        while ( i.hasNext () ) 
        {
           FileItem fi = (FileItem)i.next();
           if ( !fi.isFormField () )	
           {
              // Get the uploaded file parameters
              String fieldName = fi.getFieldName();
              String fileName = fi.getName();
              String contentType = fi.getContentType();
              boolean isInMemory = fi.isInMemory();
              long sizeInBytes = fi.getSize();
              // Write the file
              if( fileName.lastIndexOf("\\") >= 0 ){
                 file = new File( filePath + fileName.substring( fileName.lastIndexOf("\\")));
              }else{
                 file = new File( filePath + fileName.substring(fileName.lastIndexOf("\\")+1));
              }
              fi.write(file) ;
              if(fieldName.equals("thumbnail"))
              {
            	  video.setThumbnail(fileName);
              }else if(fieldName.equals("video"))
              {
            	  video.setVideo_url(fileName);
              }
              out.println("Uploaded Filename: " + fileName +  " field---- "+fieldName + " content type ----" + contentType+  "<br>");
           }
           else
           {
        	   if (fi.getFieldName().equals("title")) 
        	   {
        		   video.setTitle(fi.getString());
        	   }
        	   if (fi.getFieldName().equals("duration")) 
        	   {
        		   video.setDuration(fi.getString());
        	   }
        	   if (fi.getFieldName().equals("description")) 
        	   {
        		   video.setDescription(fi.getString());
        	   }
           }
        }
        out.println("</body>");
        out.println("</html>");
        System.out.println(file.getAbsolutePath().replace("\\", "/"));
        System.out.println("Video object : " + gson.toJson(video));
        persistance.addVideo(video);
       
        }catch(Exception ex)
        {
        	System.out.println("Upload Error : " + ex.getMessage());
        }
	}
}
